#!/bin/bash
readonly BUMP_TYPE=$1
ECHO "Bumping NPM version with $BUMP_TYPE" 
npm version $BUMP_TYPE
readonly CURRENT_VERSION=$(node -p "require('./package.json').version");
ECHO $CURRENT_VERSION
git add .
git commit -a -m "Updated version number to $CURRENT_VERSION"
git tag "$CURRENT_VERSION"
git push && git push --tags
