#!/bin/bash
PACKAGE_VERSION=$(cat package.json \
  | grep version \
  | head -1 \
  | awk -F: '{ print $2 }' \
  | sed 's/[",]//g' \
  | tr -d '[[:space:]]')
PACKAGE_NAME=$(cat package.json \
  | grep name \
  | head -1 \
  | awk -F: '{ print $2 }' \
  | sed 's/[",]//g' \
  | tr -d '[[:space:]]')
echo "Building docker image for ${PACKAGE_NAME} on version ${PACKAGE_VERSION}"
docker build -t userquest/${PACKAGE_NAME} .
echo "Build complete - tagging with ${PACKAGE_VERSION}"
docker tag userquest/${PACKAGE_NAME} userquest/${PACKAGE_NAME}:${PACKAGE_VERSION}
echo "Tag complete - publishing with ${PACKAGE_VERSION}"
docker push userquest/${PACKAGE_NAME}:${PACKAGE_VERSION}


